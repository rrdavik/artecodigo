//
//  meta.cpp
//  meta
//
//  Created by david.rodriguez on 17/9/18.
//

#include <stdio.h>
#include "ofMain.h"
#include <meta.h>
using namespace meta;

template<class T>
Terna<T>::Terna(T x, T y, T z){
    setX(x);
    setY(y);
    setZ(z);
}

void Positionable::setPosition(float x, float y, float z){
    Position *p = new Position(x,y,z);
    position=p;
}

void Rotable::setRotation(float x, float y, float z){
    Rotation* r = new Rotation(x,y,z);
    rotation = r;
}

void Shape::setColor(int r, int g, int b){
    Color* c = new Color(r,g,b);
    color = c;
}
Shape::Shape(int l, bool f, Color* c){
    lineWidth = l;
    filled = f;
    color = c;
}
/*Circle::Circle(int l, bool f, Color* c, float r, int re){
    Circle circle(l,f,c);
    circle.setRadio(r);
    circle.setResolution(re);
    return circle;
}*/


void Circle::draw(){
    
    ofPushMatrix();
//    ofTranslate(this->getX(),this->getY());
    ofSetCircleResolution(this->getResolution());
    if(this->getFilled()){
        ofFill();
    } else {
        ofNoFill();
    }
    
    Color* color = this->getColor();
    ofSetColor(color->getX(),color->getY(),color->getZ());
    
    ofDrawCircle(this->getPosition()->getX(),this->getPosition()->getY(), this->getRadio());
    ofPopMatrix();
}

void Circle::clon(){
    for(int i=0;i<this->getNClones();i++){
        int line = ofRandom(0,5);
        bool fill = ofRandom(0,1);
        int size = ofRandom(0,250);
        int resol = ofRandom(0,100);
        int deltaX = ofRandom(0,250);
        int deltaY = ofRandom(0,250);
          int r = ofRandom(0,255);
          int g = ofRandom(0,255);
          int b = ofRandom(0,255);
        Circle* circle = new Circle(line,fill,new Color(r,g,b));
        circle->setResolution(resol);
        circle->setRadio(size);
        circle->setPosition(this->getPosition()->getX()+deltaX, this->getPosition()->getY()+deltaY, 0.0);
        circle->draw();
    }
    
}

