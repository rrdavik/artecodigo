//
//  meta.h
//  meta
//
//  Created by david.rodriguez on 16/9/18.
//

#ifndef meta_h
#define meta_h

namespace meta{
    template<class T>
    class Terna{
        T x, y, z;
    public:
        Terna(T x, T y, T z);
        T getX(){ return x;}
        T getY(){ return y;}
        T getZ(){ return z;}
        void setX(T x){ this->x=x;}
        void setY(T y){ this->y=y;}
        void setZ(T z){ this->z=z;}
    };
    class Position: public Terna<float>{
    public:
        Position (float x, float y, float z):
        Terna(x,y,z){}
    };
    class Rotation: public Terna<float>{
    public:
        Rotation(float x, float y, float z):
        Terna(x,y,z){}
    };
    class Color: public Terna<int>{
    public:
        Color(int r, int g, int b):
        Terna(r,g,b){}
    };
    
    class Positionable{
        Position* position;
    public:
        void setPosition(float x, float y, float z);
        void setPosition(Position* p){ position = p; }
        Position* getPosition(){ return position; }
    };

    class Rotable{
        Rotation* rotation;
    public:
        void setRotation(float x, float y, float z);
        void setRotation(Rotation* r){ rotation = r; }
        Rotation* getRotation(){ return rotation; }
    };
    
    class Clonable{
        int nClones;
    public:
        void setNClones(int n){ this->nClones=n;}
        int getNClones(){ return nClones; }
        virtual void clon(){}
    };
    
    class Action{
 //   public:
 //       virtual void run();
    };

    class Accionable{
        vector<Action> actions;
    public:
        void pushAction();
        Action popAction();
    };

    class Sizeable{
    public:
        virtual void setSize();
    };

    class Shape: public Positionable, public Rotable, public Clonable{
        int lineWidth;
        bool filled;
        Color* color;
    public:
        Shape(){}
        Shape(int l, bool f, Color* c);
        void setLineWidth(int w){ lineWidth = w; }
        void setFilled(bool f){ filled = f; }
        bool getFilled(){ return filled; }
        void setColor(int r, int g, int b);
        Color* getColor(){ return color;}
        virtual void draw(){}
    };

    class Circle: public Shape{
        float radio;
        int resolution;
    public:
        Circle(int l, bool f, Color* c):
        Shape(l, f, c){}
      //  Circle(int l, bool f, Color* c,float r, int re);
        void draw();
        void clon();
        int getResolution(){ return resolution; }
        void setResolution(int r){ resolution = r; }
        float getRadio(){ return radio; }
        void setRadio(float r){ radio = r; }
    };

    class Rectangle: public Shape{
        float width;
        float height;
    public:
        void draw();
    };
}


#endif /* meta_h */
