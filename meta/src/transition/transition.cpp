//
//  transition.cpp
//  spiroMotion
//
//  Created by Alfredo Galiana Mora on 24/9/18.
//
//

#include "transition.hpp"
#include <math.h>

Transition::Transition(int initValue, int endValue, int nticks, int method, int mode)
{
    this->currentTick = 0;
    
    this->initValue = initValue;
    this->endValue  = endValue;
    this->nticks    = nticks;
    this->method    = method;
    this->mode      = mode;
    
    this->delta = this->endValue - this->initValue;
    this->inc   = 1.0/(this->nticks - 1);
}

float Transition::tick()
{
    if (this->currentTick < 0) {
        this->currentTick = 0;
        this->reverse = false;
    }
    
    this->value = this->delta * this->op(this->currentTick * this->inc) + this->initValue;
    
    
    if (this->currentTick < this->nticks) {
        if (this->reverse) {
            this->currentTick--;
        } else {
            this->currentTick++;
        }
    } else {
        if (this->mode == Transition::LOOP_MODE) {
            this->currentTick = 0;
        }
        
        if (this->mode == Transition::REVERSE_LOOP_MODE) {
            this->currentTick--;
            this->reverse = true;
        }
    }
}

float Transition::op(float x)
{
    switch (this->method) {
        case Transition::FUN_LINEAR:
            return x;
            break;
        case Transition::FUN_QUAD:
            return (x*x);
            break;
        case Transition::FUN_SIN:
            return sinf(x * M_PI);
            break;
        case Transition::FUN_OUT_QUAD:
            return (x*(2-x));
            break;
        case Transition::FUN_CUBIC:
            return (x*x*x);
            break;
        case Transition::FUN_ELASTIC:
            return (.04 - .04/x) * sinf(25*x) + 1;
            break;
        case Transition::FUN_ELASTIC_2:
            return (.01 - .01/x) * sinf(25*x) + 1;
            break;
        case Transition::FUN_OUT_CUBIC:
            return (x-1)*(x-1)*(x-1)+1;
            break;
        case Transition::FUN_IN_OUT_CUBIC:
            return x<.5 ? 4*x*x*x : (x-1)*(2*x-2)*(2*x-2)+1;
            break;
        case Transition::FUN_QUART:
            return (x*x*x*x);
            break;
        case Transition::FUN_IN_OUT_QUART:
            return x<.5 ? 8*x*x*x*x : 1-8*(x-1)*x*x*x;
            break;
        case Transition::FUN_IN_QUINT:
            return (x*x*x*x*x);
            break;
        case Transition::FUN_OUT_QUINT:
            return 1+(x-1)*x*x*x*x;
            break;
        case Transition::FUN_IN_OUT_QUINT:
            return x<.5 ? 16*x*x*x*x*x : 1+16*(x-1)*x*x*x*x;
            break;
        case Transition::FUN_SEMI_CIRCLE:
            return sqrtf(1-(x*x));
            break;
        default:
            return 0;
            break;
    }
}
