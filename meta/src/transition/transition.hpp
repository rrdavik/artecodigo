//
//  transition.hpp
//  spiroMotion
//
//  Created by Alfredo Galiana Mora on 24/9/18.
//
//

#ifndef transition_hpp
#define transition_hpp

#include <stdio.h>

#endif /* transition_hpp */

class Transition {
public:
    
    static const int SINGLE_MODE        = 0;
    static const int LOOP_MODE          = 1;
    static const int REVERSE_LOOP_MODE  = 2;
    
    static const int FUN_LINEAR       = 0;
    static const int FUN_QUAD         = 1;
    static const int FUN_SIN          = 2;
    static const int FUN_OUT_QUAD     = 3;
    static const int FUN_CUBIC        = 4;
    static const int FUN_ELASTIC      = 5;
    static const int FUN_ELASTIC_2    = 6;
    static const int FUN_OUT_CUBIC    = 7;
    static const int FUN_IN_OUT_CUBIC = 8;
    static const int FUN_QUART        = 9;
    static const int FUN_IN_OUT_QUART = 10;
    static const int FUN_IN_QUINT     = 11;
    static const int FUN_OUT_QUINT    = 12;
    static const int FUN_IN_OUT_QUINT = 13;
    static const int FUN_SEMI_CIRCLE  = 14;
    
    int initValue;
    int endValue;
    int delta;
    int nticks;
    int currentTick;
    float value;
    float inc;
    bool reverse;
    int method;
    int mode;
    
    float tick();
    float op(float x);
    Transition(int initValue, int endValue, int nticks, int method, int mode);
    
};
