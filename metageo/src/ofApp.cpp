#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    // RGB A(opacidad)
    ofBackground(0);
    // ofBackground(  125);
    ofSetCircleResolution(100);
    ofSetFrameRate(60);
    ofEnableAlphaBlending();
    
    // dejamos huella con el movimiento
    // forma de crear una pluma rapida
    ofSetBackgroundAuto(false);

    ofSetLineWidth(3);
    
    figure = true;
    radioC = squareS= angle = 0.0;
    hollyDotNumber=10; //ofRandom(10);
    radius = 50;
     drawedDots = myTimer = 0;
    
}

//--------------------------------------------------------------
void ofApp::update(){
    
    angle=angle+1.2;
    squareS+=2;
    radioC+=2;
    
    if (radioC>(ofGetWindowWidth()/2)+200) {
        radioC=0.0;
        squareS=0.0;
        figure=!figure;
    }
  
    if (squareS>(ofGetWindowWidth()*15)){
         figure=!figure;
         radioC=0.0;
        squareS=0.0;
    }
}

//--------------------------------------------------------------
void ofApp::draw(){

  
   
    if (figure){
        this->circleGrow();
    } else {
        this->rectangleGrow();
    }
  //  this->hollyBackground();
    
    
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

void ofApp::clear(){
    ofSetRectMode(OF_RECTMODE_CORNER);
     ofTranslate(0, 0);
    ofFill();
    ofSetColor(0);
    ofDrawRectangle(0, 0, ofGetWindowWidth(), ofGetWindowHeight());
}

void ofApp::circleGrow(){
    
    
    ofPushMatrix();
    this->clear();
     ofTranslate(ofGetWindowWidth()/2, ofGetWindowHeight()/2);
  //    ofSetCircleResolution(25);
    ofNoFill();
    ofSetColor(255);
    ofDrawCircle(0, 0, radioC);
    ofPopMatrix();
}

void ofApp::rectangleGrow(){
    

    ofPushMatrix();
    this->clear();
    ofSetRectMode(OF_RECTMODE_CENTER);
    ofTranslate(ofGetWindowWidth()/2, ofGetWindowHeight()/2);
    ofRotateZ(angle);
    ofNoFill();
    ofSetColor(255);
    ofDrawRectangle(0, 0,squareS, squareS);
    ofPopMatrix();
    
}

void ofApp::hollyBackground(){
    myTimer = ofGetElapsedTimeMillis();
    
    ofFill();
    ofSetColor(255);
    
    if ((myTimer > 20000 ) && (drawedDots<hollyDotNumber)){
        ofTranslate(ofRandom(0.0, ofGetWindowWidth()),
                    ofRandom(0.0, ofGetWindowHeight()));
        
      //  for(int j=0;j<radius;j++){
        ofDrawCircle(0, 0, radius);
    
       // }
        drawedDots++;
    }
      ofLog() << myTimer;
    
}
